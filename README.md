# Firmware environment
Firmware development environment using real hardware outputs.

## Setup:
```bash
git submodule update --init --recursive
./setup.sh
```

## Pulling changes:
```bash
git submodule foreach git pull origin master
```

## Usage:
Connect arduino and 3D printer, by deps/StepCapture/README.md

Start sniffer
```bash
./run.sh [PORT] [BAUDRATE]
```

Start output reader
```bash
./start.sh
```
