#!/bin/bash

INPUT_FILE=step_count

tail -f tmp/$INPUT_FILE | ./utils/newliner | ./utils/binary_pad.sh | ./utils/gcodify | ./utils/scaler | tee tmp/gcode | python deps/Printator/printator.py -in -ig
