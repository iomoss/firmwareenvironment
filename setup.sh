#!/bin/sh

mkdir -p utils

# Setup pos_processor
cd deps/pos_processor
make
cd ../..
mv deps/pos_processor/main utils/gcodify

# Setup gcode_scaler
cd deps/gcode_scaler
make
cd ../..
mv deps/gcode_scaler/main utils/scaler

# Setup newliner
cd deps/newliner
make
cd ../..
mv deps/newliner/main utils/newliner

# Setup bash scripts
cp scripts/* utils
