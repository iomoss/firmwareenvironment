#!/bin/sh

PORT=$1
BAUDRATE=$2
OUTPUT_FILE=step_count

if [ -z "$PORT" ]; then
    PORT=/dev/ttyUSB1
fi
if [ -z "$BAUDRATE" ]; then
    BAUDRATE=115200
fi

mkdir -p tmp/
rm -f tmp/$OUTPUT_FILE
minicom -D $PORT -b $BAUDRATE -H -C tmp/$OUTPUT_FILE
