#!/bin/bash

# Read out steps
while read line
do
    BINARY=$(echo "obase=2; ibase=16; $line" | bc)
    printf "%06d\n" $BINARY
done < "${1:-/dev/stdin}"

